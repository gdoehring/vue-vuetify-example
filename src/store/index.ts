import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    todos: [
      "Milch kaufen",
      "Katzen füttern",
      "Surfen gehen",
      "Milch kaufen",
      "Katzen füttern",
      "Surfen gehen",
      "Milch kaufen",
      "Katzen füttern",
      "Surfen gehen"
    ]
  },
  mutations: {
    addTodo: (state, newTodo: string) => {
      state.todos.push(newTodo);
    }
  },
  actions: {
    addTodoToServer: (context, newTodo: string) => {
      const promise = new Promise(resolve => {
        // code upload
        setTimeout(() => {
          context.commit("addTodo", newTodo);
          resolve(true);
        }, 3000);
      });

      return promise;
    }
  },
  modules: {}
});
